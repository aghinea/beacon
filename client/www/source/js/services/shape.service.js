(function geoLocation(angular) {
  'use strict';

  const API_ROOT = 'http://www.dinkorporated.com/gpstest/';
  const API_ACTION = {
    CREATE: 'create',
    GET: 'get',
    LIST: 'list_records'
  };

  angular
    .module('beacon.services', [])
    .factory('$shape', shape);

  function shape($http) {
    return {
      createShape: createShape,
      getShapeById: getShapeById,
      getShapesList: getShapesList,
      getCurrentLocation: getCurrentLocation
    };

    /**
     * @description
     * Saves a `Shape` object to the server;
     *
     * @param {Object} shape
     * @param {Promise=} abortPromise Promise to abort ongoing request.
     * @returns {Promise<Object>} Promise for the `Shape`.
     */
    function createShape(shape, abortPromise) {
      var data = angular.extend({
        action: API_ACTION.CREATE
      }, shape);
      return $http.post(API_ROOT, data);
    }

    /**
     * @description
     * Gets `Shape` form cache or via $http.
     *
     * @param {Number} shapeId
     * @param {Promise=} abortPromise
     * @returns {Promise<Object>=} abortPromise
     */
    function getShapeById(shapeId, abortPromise) {
      return $http({
        method: 'GET',
        url: API_ROOT + '?' + 'action=' + API_ACTION.GET + '&' + 'id=' + shapeId,
        timeout: abortPromise
      });
    }

    /**
     * @description
     * Gets `ShapeList` form cache or via $http.
     *
     * @param {Promise=} abortPromise Promise to abort ongoing request.
     * @returns {Promise<Object>} Promise for the `ShapeList`.
     */
    function getShapesList(abortPromise) {
      return $http({
        method: 'GET',
        url: API_ROOT + '?' + 'action=' + API_ACTION.LIST,
        timeout: abortPromise
      });
    }

    /**
     * @param {Promise=} abortPromise
     * @returns {Promise<Object>} Promise for the `Location`.
     */
    function getCurrentLocation(abortPromise) {
      console.info('locationService', 'Should get location ...');
    }
  }

})(window.angular);
