(function Camera(angular, navigator) {
    'use strict';

    angular.module('beacon.services', [])
        .factory('$camera', cameraService);

    function cameraService($q) {
        return {
            getPicture(options) {
                const $deffered = $q.defer();

                navigator.camera
                    .getPicture((result) => {
                        $deffered.resolve(result);
                    }, (error) => {
                        $deffered.reject(error);
                    }, options);

                return $deffered.promise;
            }
        }
    }

})(window.angular, window.navigator);



