(function (angular) {
  'use strict';

  angular.module('beacon.constants', [])
    .constant(beaconConstants);

  function beaconConstants() {
    return { title: 'Beacon' };
  }

})(window.angular);
