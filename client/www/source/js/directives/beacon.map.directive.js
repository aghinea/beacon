(function Map(angular) {
  'use strict';

  const LEAFLET_TILESERVER = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';
  const LEAFLET_DEFAULT_LOCATION = [45, 25];

  angular.module('beacon.directives', [])
    .directive('beaconMap', beaconMap);

  function beaconMap() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        mapInstance: '=', //@Object
        mapLocation: '<', //@Array<Number>
        mapShape: '<',  //@Array<Object>
        mapMarker: '<'
      },
      link: mapComponentControllerLink
    };

    function mapComponentControllerLink($scope, $attrs) {
      const LEAFLET_GRAYSCALE = L.tileLayer(LEAFLET_TILESERVER, {id: 'mapbox.light'});

      const _attributes = $attrs[0];
      const _defaultLocation = _getDefaultLocation($scope.mapLocation);

      //Setup `popup`
      const _popupContentStartingLocation = undefined;
      const _popupContent = undefined;

      $scope.mapInstance = L.map(_attributes.id, {
        center: _defaultLocation,
        zoom: 10,
        reuseTiles: true,
        detectRetina: true,
        zoomControl: false,
        zIndex: 1,
        layers: [LEAFLET_GRAYSCALE]
      });

      if ($scope.mapMarker) {

        //Setup `popup`
        _popupContentStartingLocation = `<p><strong>Starting Location</strong> <badge>${JSON.stringify(_defaultLocation)}</badge></p>`;
        _popupContent = _popupContentStartingLocation;

        $scope.marker = L.marker(_defaultLocation);
        $scope.marker.bindPopup(_popupContent);
        $scope.marker.addTo($scope.mapInstance);

        $scope.marker.openPopup();
      }

      if ($scope.mapShape) {
        new L.Polyline($scope.mapShape, {
          color: 'red',
          weight: 3,
          opacity: 0.5,
          smoothFactor: 1
        }).addTo($scope.mapInstance);

        $scope.mapInstance.fitBounds($scope.mapShape);
      }
    }
  }

  /**
   * @private
   * @description
   * Gets the center of the map
   *
   * @param {Array<Number} locationData
   * returns {Array<Number>}
   */
  function _getDefaultLocation(locationData) {
    if (locationData && angular.isArray(locationData) && locationData.length === 2) {
      return locationData;
    }

    return LEAFLET_DEFAULT_LOCATION;
  }

})(window.angular);
