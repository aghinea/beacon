(function (angular) {
  'use strict';

  /**
   * @namespace `beacon.locationsCtrl`
   * @type Controller
   *
   */
  angular.module('beacon.locationsCtrl', [])
    .controller('LocationsViewCtrl', ['$scope', '$state', '$shapeListResolve', LocationsViewCtrl]);

  function LocationsViewCtrl($scope, $state, $shapeListResolve) {
    $scope.shapeList = $shapeListResolve.data;

    $scope.openShape = function openLocation(shapeItemId) {
      $state.go('menu.item', {id: shapeItemId});
    }
  }

})(window.angular);
