(function (angular, moment) {
  'use strict';

  /**
   * @namespace `beacon.locationCtrl`
   * @type Controller
   *
   */
  angular.module('beacon.locationCtrl', [])
    .controller('LocationViewCtrl', ['$scope', '$shapeResolve', LocationViewCtrl]);

  function LocationViewCtrl($scope, $shapeResolve) {
    var momentFormatted = undefined;
    $scope.shape = $shapeResolve.data;

    //Formatted data:
    momentFormatted = moment(new Date(Number($scope.shape.timestamp)));

    $scope.shape.timestamp = momentFormatted.format('MMMM Do YYYY, h:mm:ss a');
    $scope.shape.timeago = momentFormatted.fromNow();
    $scope.shapeDefaultLocation = [$scope.shape.shapeData[0].lat, $scope.shape.shapeData[0].lng];
    $scope.map = undefined;
  }

})(window.angular, window.moment);
