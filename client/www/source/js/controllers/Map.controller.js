(function (angular, moment, _) {
  'use strict';

  const SHOW_BACKDROP = true;
  const POPUP_TIMEOUT = 3000;
  const SERVICE_INTERVAL = 2300;
  const DEFAULT_LOCATION = [45, 25];

  const MARKER_TYPE_TRACKING = 'tracking';
  const MARKER_TYPE_CENTER = 'center';
  const UNTITLED_SHAPE = 'Untitled shape';

  const PICTURE_DEFAULTS = {
    quality: 75,
    targetWidth: 200,
    targetHeight: 200,
    sourceType: 1
  };

  /**
   * @namespace `beacon.mapCtrl`
   * @type Controller
   *
   */
  angular.module('beacon.mapCtrl', [])
    .controller('MapCtrl', MapCtrl);

  MapCtrl.$inject = ['$scope', '$q', '$injector', '$interval'];

  function MapCtrl($scope, $q, $injector, $interval) {
    const vm = this;

    vm.title = 'Beacon';

    vm.$injector = $injector;
    vm.$httpCancelerDeferred = $q.defer();

    vm.imageData = undefined;
    vm.loading = undefined;
    vm.popup = undefined;
    vm.map = undefined;

    vm.lastUpdateTime = _updateServiceTime();
    vm.serviceInterval = undefined;
    vm.serviceAccuracy = 0;

    vm.shapeName = UNTITLED_SHAPE;
    vm.shapeData = [];

    $scope.$on('$destroy', _onScopeDestroy);

    function _onScopeDestroy() {
      $interval.cancel(vm.serviceInterval);
    }
  }

  MapCtrl.prototype.centerOnMe = centerOnMe;
  MapCtrl.prototype.manageTrackingService = manageTrackingService;
  MapCtrl.prototype.startTracking = startTracking;
  MapCtrl.prototype.stopTracking = stopTracking;
  MapCtrl.prototype.getPicture = getPicture;

  function getPicture() {
    const vm = this;
    const $camera = vm.$injector.get('$camera', 'MapCtrl');
    const $ionicPopup = vm.$injector.get('$ionicPopup', 'MapCtrl');

    $camera.getPicture(PICTURE_DEFAULTS).then(imageData => {
      vm.imageData = imageData;;
    }, _errorGetPicture);

    /**
    * @description
    * Handles the navigator error callback
    *
    * @param {Object} error
    */
    function _errorGetPicture(error) {
      _handleLoading.call(vm);

      vm.popup = $ionicPopup.alert({
        title: 'Camera Error',
        template: 'The picture could not be loaded!'
      });
    }
  }


  /**
   * @public
   * @description
   * Starts or stops the tracking service
   *
   * @returns void
   */
  function manageTrackingService() {
    const vm = this;

    if (!vm.serviceInterval) {
      vm.startTracking();
      return;
    }

    if (!!vm.serviceInterval) {
      vm.stopTracking();
    }
  }


  /**
   * @public
   * @description
   * Starts the tracking service
   *
   * @returns void
   */
  function startTracking() {
    const vm = this;
    const $ionicLoading = vm.$injector.get('$ionicLoading', 'MapCtrl');
    const $interval = vm.$injector.get('$interval', 'MapCtrl');
    const $state = vm.$injector.get('$state', 'MapCtrl');
    const shapeName = _requestShapeName.call(vm);

    if (Boolean(vm.serviceInterval)) {
      stopTracking.call(vm);
      return;
    }

    if (!vm.map || !shapeName) {
      //Loader hide:
      $ionicLoading.hide();
      return;
    }

    //Write `shapeName` to ModelView
    vm.shapeName = shapeName;

    //Go to `tracking` screen
    $state.go('menu.tracking');

    vm.loading = $ionicLoading.show({
      showBackdrop: SHOW_BACKDROP
    });

    //Start service
    vm.serviceInterval = $interval(_track.bind(vm), SERVICE_INTERVAL);

    _addTrackingMarker(vm.map);

    /**
     * @private
     * @param {L.map} map
     *
     * @description
     * Add marker
     * @returns void
     */
    function _addTrackingMarker(map, marker) {

      //Clear layers of markers either `MARKER_TYPE_TRACKING` or `MARKER_TYPE_CENTER`
      map.eachLayer(layer => {
        if (layer.options.type === MARKER_TYPE_TRACKING || layer.options.type === MARKER_TYPE_CENTER) {
          map.removeLayer(layer);
        }
      });

      vm.marker = L.marker(DEFAULT_LOCATION, { type: MARKER_TYPE_TRACKING });
      vm.marker.bindPopup(JSON.stringify(DEFAULT_LOCATION));
      vm.marker.addTo(map);
    }
  }

  /**
   * @public
   * @description
   * Stops the tracking service
   *
   * @returns void
   */
  function stopTracking() {
    const vm = this;

    const $ionicLoading = vm.$injector.get('$ionicLoading', 'MapCtrl');
    const $geoLocation = vm.$injector.get('$geoLocation', 'MapCtrl');
    const $ionicPopup = vm.$injector.get('$ionicPopup', 'MapCtrl');
    const $timeout = vm.$injector.get('$timeout', 'MapCtrl');
    const $interval = vm.$injector.get('$interval', 'MapCtrl');

    vm.loading = $ionicLoading.show({
      showBackdrop: SHOW_BACKDROP
    });

    $geoLocation.createShape({
      timestamp: vm.timestamp,
      shapeName: vm.shapeName,
      shapeData: vm.shapeData
    }, vm.$httpCancelerDeferred)
      .then(_successCreateShapeHandler)
      .catch(_errorCreateShapeHandler);

    /**
     * @private
     * @description
     * Success save handler
     *
     * @returns void
     */
    function _successCreateShapeHandler() {
      //Hide any loader
      _handleLoading.call(vm);

      //Show success message
      vm.popup = $ionicPopup.show({
        title: 'Success',
        subTitle: 'Shape successfully saved!'
      });

      //Close success message implicitely
      $timeout(function _autoClose() {
        vm.popup.close();
      }, POPUP_TIMEOUT);

      _clear.call(vm);
    }

    /**
     * @private
     * @description
     * Error save handler
     *
     * @returns void
     */
    function _errorCreateShapeHandler() {
      //Hide any loader
      _handleLoading.call(vm);

      //Clear
      _clear.call(vm);

      vm.popup = $ionicPopup.alert({
        title: 'Saving Error',
        template: 'There was an error while trying to save the shape!'
      });
    }

    /**
     * @private
     * @description
     * Cleans current shape request
     * @return void
     */
    function _clear() {

      if (vm.map && vm.map.hasLayer(vm.marker)) {
        vm.map.removeLayer(vm.marker);
      }

      vm.timestamp = undefined;
      vm.shapeName = UNTITLED_SHAPE;
      vm.shapeData = [];

      $interval.cancel(vm.serviceInterval);

      vm.serviceInterval = undefined;
    }
  }

  /**
   * @public
   * @description
   * Centers the map on the `current` location
   * @returns void
   */
  function centerOnMe() {
    var vm = this;
    var $ionicLoading = vm.$injector.get('$ionicLoading', 'MapCtrl');

    if (Boolean(vm.serviceInterval)) {
      return;
    }

    vm.loading = $ionicLoading.show({
      showBackdrop: SHOW_BACKDROP
    });

    _addCenterMarker(vm.map);

    /**
     * @private
     * @param {L.map} map
     *
     * @description
     * Add marker to the center
     * @returns void
     */
    function _addCenterMarker(map) {

      map.eachLayer(layer => {
        if (layer.options.type === MARKER_TYPE_CENTER) {
          map.removeLayer(layer);
        }
      });

      vm.marker = L.marker(DEFAULT_LOCATION, { type: MARKER_TYPE_CENTER });
      vm.marker.bindPopup(JSON.stringify(DEFAULT_LOCATION));
      vm.marker.addTo(map);
    }

    _track.call(vm);
  }

  /**
   * @private
   * @description
   *  - Invokes the `Navigator` API (Application Programmable Interface)
   *
   * @returns void
   */
  function _track() {
    const vm = this;
    const $window = vm.$injector.get('$window', 'MapCtrl');
    const $ionicPopup = vm.$injector.get('$ionicPopup', 'MapCtrl');

    const _latLngPrecision = 7;
    const _optionsShapeHandler = {
      enableHighAccuracy: true,
      maximumAge: Infinity
    };

    if (!$window.navigator) {
      return;
    }

    //Update last time update
    vm.lastUpdateTime = _updateServiceTime();

    //Resolve position
    $window
      .navigator
      .geolocation
      .getCurrentPosition(
      _successCreateShapeHandlerCallback,
      _errorCreateShapeHandlerHandler,
      _optionsShapeHandler);

    /**
     * @description
     * Handles the navigator success callback
     *
     * @param {Object} pos
     */
    function _successCreateShapeHandlerCallback(pos) {
      var _currentPosition = _GeoFormatter(pos);
      var _latitude = _currentPosition.latitude;
      var _longitude = _currentPosition.longitude;

      var _altitude = _currentPosition.altitude ? parseInt(_currentPosition.altitude, 10) : 0; //meters
      var _accuracy = _currentPosition.accuracy ? parseInt(_currentPosition.accuracy, 10) : 0;
      var _latLng = new L.LatLng(_.ceil(_latitude, _latLngPrecision), _.ceil(_longitude, _latLngPrecision));

      //Setup `popup`
      var _popupContentLocation = '<p><strong>Location</strong> <badge>' + JSON.stringify(_latLng) + '</badge></p>';
      var _popupContentAccuracy = '<p><strong>Accuracy</strong> <badge>' + _accuracy + '%</badge></p>';
      var _popupContentAltitude = '<p><strong>Altitude</strong> <badge>' + _altitude + ' meters<badge></p>';
      var _popupContent = [_popupContentLocation, _popupContentAccuracy, _popupContentAltitude].join('');

      //Setup `timestamp`
      vm.timestamp = pos.timestamp;

      //Setup `accuracy`
      vm.serviceAccuracy = _accuracy;

      //Setup `shapeData`
      vm.shapeData.push({
        lat: _latitude,
        lng: _longitude
      });

      //Set `vm.map` centered
      vm.map.panTo(_latLng);

      //Update `vm.marker`
      if (vm.marker) {
        vm.marker.getPopup().setContent(_popupContent);
        vm.marker.setLatLng(_latLng);
        vm.marker.openPopup();
      }

      //Close loading
      _handleLoading.call(vm);
      console.log('Current position', _currentPosition);
    }

    /**
     * @description
     * Handles the navigator error callback
     *
     * @param {Object} error
     */
    function _errorCreateShapeHandlerHandler(error) {
      var errorTitle = 'Error';
      vm.shapeData = [];
      _handleLoading.call(vm);

      switch (error.code) {
        case error.PERMISSION_DENIED:

          vm.popup = $ionicPopup.alert({
            title: errorTitle,
            template: 'User denied the request for Geolocation.'
          });
          return;

        case error.POSITION_UNAVAILABLE:

          vm.popup = $ionicPopup.alert({
            title: errorTitle,
            template: 'Location information is unavailable.'
          });
          return;

        case error.TIMEOUT:

          vm.popup = $ionicPopup.alert({
            title: errorTitle,
            template: 'The request to get user location timed out.'
          });
          return;

        case error.UNKNOWN_ERROR:

          vm.popup = $ionicPopup.alert({
            title: errorTitle,
            template: 'An unknown error occurred.'
          });
          return;
      }
    }
  }

  /**
   * @helper
   * @private
   * @description
   * Extracts the coordinates from a `Navigator` object.
   *
   * @param {Object} position
   * @return {Object}
   */
  function _GeoFormatter(position) {
    return position.coords;
  }

  /**
   * @helper
   * @private
   * @description
   * Validates `vm.shapeName`
   */
  function _requestShapeName() {
    var vm = this;
    var promptDataInput = prompt('Please provide the shape name:');

    if (promptDataInput === null) {
      return;
    }

    if (promptDataInput && promptDataInput.length > 0) {
      return _.trim(promptDataInput);
    }
  }

  /**
   * @helper
   * @private
   * @description
   * Hides popup
   */
  function _handleLoading() {
    var vm = this;
    var $ionicLoading = vm.$injector.get('$ionicLoading', 'MapCtrl');
    $ionicLoading.hide();
  }

  /**
   * @helper
   * @private
   * @description
   * Formats a date with `moment`
   */
  function _updateServiceTime() {
    return moment(new Date()).fromNow();
  }

})(window.angular, window.moment, window._);
