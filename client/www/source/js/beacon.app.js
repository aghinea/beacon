(function App(angular) {
  'use strict';

  angular.module('beacon', [
    'ionic',
    'beacon.configHttp',
    'beacon.configRoutes',
    'beacon.constants',
    'beacon.mapCtrl',
    'beacon.locationCtrl',
    'beacon.locationsCtrl',
    'beacon.services',
    'beacon.directives'
  ]);

  angular.module('beacon').run($ionicPlatform => {
      $ionicPlatform.ready(() => {
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }
      });
    });

})(window.angular);

