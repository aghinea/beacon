(function (angular) {
  'use strict';

  angular.module('beacon.configRoutes', [])
    .config(beaconRoutesConfig);

  beaconRoutesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

  function beaconRoutesConfig($stateProvider, $urlRouterProvider) {

    const $rootState = 'menu';
    const templateUrl = 'templates/';

    $stateProvider
      .state($rootState, {
        url: '',
        abstract: true,
        templateUrl: 'templates/event-menu.html',
        controller: 'MapCtrl as vm'
      })
      .state($rootState + '.tracking', {
        url: '/tracking',
        views: {
          menuContent: {
            templateUrl: templateUrl + 'tracking.html'
          }
        }
      })
      .state($rootState + '.location', {
        url: '/location',
        views: {
          menuContent: {
            controller: 'LocationsViewCtrl',
            templateUrl: templateUrl + 'location.html'
          }
        },
        resolve: { $shapeListResolve: shapeListResolver }
      })
      .state($rootState + '.item', {
        url: '/location/{id}',
        views: {
          menuContent: {
            controller: 'LocationViewCtrl',
            templateUrl: templateUrl + 'locationItem.html'
          }
        },
        resolve: { $shapeResolve: shapeResolver }
      });

    $urlRouterProvider.otherwise('/tracking');

    function shapeListResolver(shape) {
      return shape.getShapesList();
    }

    function shapeResolver($stateParams, shape) {
      return shape.getShapeById($stateParams.id);
    }

  }
})(window.angular);
