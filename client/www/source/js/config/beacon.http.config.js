(function (angular) {
  'use strict';

  angular.module('beacon.configHttp', [])
    .config(beaconHttpConfig);

  beaconHttpConfig.$inject = ['$httpProvider'];

  function beaconHttpConfig($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
  }
})(window.angular);
