<?php 

	defined('STORE_FOLDER') 		or define('STORE_FOLDER', __DIR__ . '/store_folder/');
	defined('STORE_FILE_TEMPLATE') 	or define('STORE_FILE_TEMPLATE', '_shape_file_%name%.json');


	/* ---------------------------------------------- */
	/* 				CORS.							  */
	/* ---------------------------------------------- */
	header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
	header("Access-Control-Allow-Credentials: true");
	header('Access-Control-Allow-Methods: *');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Cache-Control, Content-Type, Content-Range, Content-Disposition, Content-Description');


	/* ---------------------------------------------- */
	/* 				OPTIONS REQUEST.				  */
	/* ---------------------------------------------- */
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		exit;

	}


	// get the raw POST data
    $rawData = file_get_contents("php://input");

	if( empty($rawData) == false ) {
		$_POST =  json_decode($rawData, true);
	}

	/* Helpers. */
	function response($data, $statusCode = 200) {
		header("Content-Type: application/json; charset=utf-8");
		header("HTTP/1.0 {$statusCode}");

		echo json_encode($data);
	}


	function getRequestParam($name, $default = null) {
		$value = $default;


		if( isset( $_GET[ $name ] ) ) {
			$value = $_GET[ $name ];
		}


		if( isset( $_POST[ $name ] ) ) {
			$value = $_POST[ $name ];
		}

		return $value;
	}

	function dirToArray($dir) {	  
	   $result 	= array();
	   $cdir 	= scandir($dir);


	   foreach ($cdir as $key => $value) {

			if( !in_array($value, array(".", "..")) ) {

				if( is_dir($dir . DIRECTORY_SEPARATOR . $value) ) {

					$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);

				} else {

					$result[] = $value;

				}

			}

	   }

	   return $result;
	}


	function getShapeDataContent( $file_name ) {
		$value 		= array();
		$file 		= STORE_FOLDER . $file_name;
		$content 	= file_get_contents($file);
		$value 		= json_decode($content, true);

		return $value;
	}


	/* We always response with a JSON, this is API Power :) */
	$action = getRequestParam('action');
 

	switch($action) {
		case 'list_records':
			$records 	= array();
			$files 		= dirToArray(STORE_FOLDER);

			foreach($files as $file) {
				$content = getShapeDataContent($file);

				$records[] = array(
					'id' 		=> pathinfo($file, PATHINFO_FILENAME),
					'name' 		=> $content['shapeName'],
					'timestamp' => $content['timestamp'],
					'shapeData' => $content['shapeData']
				);
				
			}


			usort($records, function($a, $b) {
				return $a['timestamp'] < $b['timestamp'];
			});


			response($records);
		break;

		case 'get':
			$id = getRequestParam('id');


			if( empty($id) == false ) {

				$data = getShapeDataContent($id . '.json');

				response($data);

			} else {
				
				
				response(array(
					'error' => 'ID is empty'
				), 500);
				
			}
		break;

		case 'create':
			$shape_name = trim( getRequestParam('shapeName') );
			$timestamp 	= trim( getRequestParam('timestamp') );
			$shape_data = getRequestParam('shapeData');


			if( empty($shape_name) == false && empty($timestamp) == false ) {


				$data_structure = array(
					'shapeName' => $shape_name,
					'timestamp' => $timestamp,
					'shapeData' => $shape_data
				);


				$file_name 	= md5( $shape_name . '_' .  $timestamp);
				$file 		= STORE_FOLDER . str_replace('%name%', $file_name, STORE_FILE_TEMPLATE);
				$fp 		= fopen($file, "w");


				fwrite($fp, json_encode($data_structure));
				fclose($fp);
				
				
				response(array(
					'success' => true
				));


			} else {

				response(array(
					'error' => 'Te rog eu mult, dar te rooog... da-mi si mie numele shape-ului.'
				), 500);

			}
		break;

		default:
			response(array(
				'time' => date('Y-m-d H:i:s')
			));
		break;	
	}

?>